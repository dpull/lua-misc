package.path = "scripts/?.lua;" .. package.path
local tableex = require("tableex")

local function test_table_len()
    local tb = {1, 2, 3, 4, 5, 6, 7, 8}
    print("1", #tb)

    tb[8] = nil
    print("2", #tb)

    tb[4] = nil
    print("3", #tb)

    tb[9] = 9
    tb[16] = 16
    tb[8] = 8
    print("4", #tb)
end

local function test_table_ipairs()
    local ipairs_print = function(desc, tb)
        print(desc)
        for i, v in ipairs(tb) do
            print(i, v)
        end   
    end

    local tb = {1, 2, 3, 4, 5, 6, 7, 8}
    ipairs_print("\n\n1", tb)

    tb[8] = nil
    ipairs_print("\n\n2", tb)

    tb[4] = nil
    ipairs_print("\n\n3", tb)

    tb[9] = 9
    tb[16] = 16
    tb[8] = 8
    ipairs_print("\n\n4", tb)

    tb[4] = 4
    ipairs_print("\n\n5", tb)
end

local function test_table_next()
    local tb = {a = 1, b = 2}

    local k, v = next(tb)
    print("1", k, v)

    tb[k] = nil
    local k1, v1 = next(tb, k)
    print("2", k1, v1)

    k1, v1 = next(tb, k)
    print("3", k1, v1)

    tb.c = 3
    k1, v1 = next(tb, k)
    print("4", k1, v1)
end

local function test_table()
    local tb = {}
    tb[0] = "7"
    tb[4] = "8"
    tb[2] = "9"
    tableex.tree(tb)
end

-- test_table_len()
-- test_table_ipairs()
-- test_table_next()
test_table()