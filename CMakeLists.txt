cmake_minimum_required(VERSION 3.6)

set(CMAKE_SUPPRESS_REGENERATION TRUE CACHE BOOL "Disable Zero Check Project")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set (CMAKE_CXX_STANDARD 11)
set (CMAKE_C_STANDARD 11)

if(NOT MSVC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -fpermissive -fsanitize=address")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -fpermissive -fsanitize=address")
endif(MSVC)

set(PROJ_NAME "lua-detail")
project(${PROJ_NAME})

set(LUA_SRC_PATH "lua")
set(LUA_SRC_IGNORE "${LUA_SRC_PATH}/lua.c;${LUA_SRC_PATH}/luac.c;${LUA_SRC_PATH}/lbitlib.c")
aux_source_directory(${LUA_SRC_PATH} LUA_SRC)
list(REMOVE_ITEM LUA_SRC ${LUA_SRC_IGNORE})
include_directories(${PROJ_NAME} ${LUA_SRC_PATH})

set(C_UTILS_PATH "utils")
add_subdirectory(${C_UTILS_PATH})
include_directories(${PROJ_NAME} ${C_UTILS_PATH})

aux_source_directory("main" MAIN_SRC)

add_executable(${PROJ_NAME} ${LUA_SRC} ${MAIN_SRC})
target_link_libraries(${PROJ_NAME} "c_utils")
