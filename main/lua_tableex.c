#include "lua_tableex.h"
#include "lstate.h"
#include "lobject.h"
#include "ltable.h"
#include "debug_tree.h"
#include <assert.h>

static Table *lua_totable(lua_State *L, int idx) {
    assert(idx > 0);
    TValue *v = s2v(L->ci->func + 1);
    Table *t = hvalue(v);
    return t;
}

static const char *strTValue(const TValue *o) {
    static char buffer[128];
    int type = ttypetag(o);
    switch (type) {
        case LUA_TNIL:
            return "nil";
        case LUA_TNUMINT:
            snprintf(buffer, sizeof(buffer), "%lld", (int64_t)ivalue(o));
            return buffer;
        case LUA_TNUMFLT:
            snprintf(buffer, sizeof(buffer), "%f", fltvalue(o));
            return buffer;
        case LUA_TBOOLEAN:
            return bvalue(o) ? "true" : "false";
        case LUA_TLIGHTUSERDATA:
            return "userdata";
        case LUA_TLCL:
        case LUA_TLCF:
        case LUA_TCCL:
            return "function";
        case LUA_TLNGSTR:
            return getstr(tsvalue(o));
        case LUA_TSHRSTR:
            return getstr(tsvalue(o));
        default:
            if ((type & 0x0F) == 0)
                return "empty";
            return "unknown";
    }
}

static const char * strNode(const Node *n) {
    static char buffer[128];
    int type = withvariant(keytt(n));
    switch (type) {
        case LUA_TNIL:
            return "nil";
        case LUA_TNUMINT:
            snprintf(buffer, sizeof(buffer), "%lld", (int64_t)keyival(n));
            return buffer;
        case LUA_TNUMFLT:
            snprintf(buffer, sizeof(buffer), "%f", fltvalueraw(keyval(n)));
            return buffer;
        case LUA_TBOOLEAN:
            return bvalueraw(keyval(n)) ? "true" : "false";
        case LUA_TLIGHTUSERDATA:
            return "userdata";
        case LUA_TLCL:
        case LUA_TLCF:
        case LUA_TCCL:
            return "function";
        case LUA_TLNGSTR:
            return getstr(keystrval(n));
        case LUA_TSHRSTR:
            return getstr(keystrval(n));
        default:
            if ((type & 0x0F) == 0)
                return "empty";
            return "unknown";
    }
}

#define hashpow2(t,n)           (lmod((n), sizenode(t)))
#define hashstr(t,str)          hashpow2(t, (str)->hash)
#define hashboolean(t,p)        hashpow2(t, p)
#define hashint(t,i)            hashpow2(t, i)
#define hashmod(t,n)            (((n) % ((sizenode(t)-1)|1)))
#define hashpointer(t,p)        hashmod(t, point2uint(p))

#if !defined(l_hashfloat)
static int l_hashfloat (lua_Number n) {
    int i;
    lua_Integer ni;
    n = l_mathop(frexp)(n, &i) * -cast_num(INT_MIN);
    if (!lua_numbertointeger(n, &ni)) {  /* is 'n' inf/-inf/NaN? */
        lua_assert(luai_numisnan(n) || l_mathop(fabs)(n) == cast_num(HUGE_VAL));
        return 0;
    }
    else {  /* normal case */
        unsigned int u = cast_uint(i) + cast_uint(ni);
        return cast_int(u <= cast_uint(INT_MAX) ? u : ~u);
    }
}
#endif

static int hash(const Table *t, const Node *n) {
    const Value *kvl = &keyval(n);
    switch (withvariant(keytt(n))) {
        case LUA_TNUMINT:
            return hashint(t, ivalueraw(*kvl));
        case LUA_TNUMFLT:
            return hashmod(t, l_hashfloat(fltvalueraw(*kvl)));
        case LUA_TSHRSTR:
            return hashstr(t, tsvalueraw(*kvl));
        case LUA_TLNGSTR:
            return hashpow2(t, luaS_hashlongstr(tsvalueraw(*kvl)));
        case LUA_TBOOLEAN:
            return hashboolean(t, bvalueraw(*kvl));
        case LUA_TLIGHTUSERDATA:
            return hashpointer(t, pvalueraw(*kvl));
        case LUA_TLCF:
            return hashpointer(t, fvalueraw(*kvl));
        default:
            return hashpointer(t, gcvalueraw(*kvl));
    }
}


static int luatable_tree(lua_State *L) {
    Table *t = lua_totable(L, 1);
    struct debug_tree *root = debug_tree_create(NULL, "*");
    
    debug_tree_create(root, "flags:%X", (unsigned)t->flags);
    struct debug_tree *array = debug_tree_create(root, "array\tsize:%u", t->sizearray);
    struct debug_tree *node = debug_tree_create(root, "node\tlog2 size:%X = %u", (unsigned)t->lsizenode, sizenode(t));
    debug_tree_create(root, "lastfree:%p, offset:%u", t->lastfree, t->lastfree ? t->lastfree - t->node : 0);
    
    for (unsigned i = 0; i < t->sizearray; ++i) {
        TValue *o = &t->array[i];
        debug_tree_create(array, "key:[%u]\tvalue:[%s]", i + 1, strTValue(o));
    }
    
    for (unsigned i = 0; i < sizenode(t); ++i) {
        struct debug_tree *parent = node;
        struct debug_tree *tmp = NULL;
        Node *n = gnode(t, i);
     
        for (;;) {
            tmp = debug_tree_create(parent, "[%d]\tnode:%p\tkey:[%s]\tvalue:[%s]\thash:[%d]", n - t->node, n, strNode(n), strTValue(gval(n)), hash(t, n));
            
            if (parent == node) {
                parent = tmp;
            }

            int nx = gnext(n);
            if (nx == 0)
                break;
            n += nx;
        }
    }
    
    debug_tree_print(root, stdout);
    debug_tree_destroy(root);
    return 1;
}

LUAMOD_API int luaopen_tableex(lua_State *L) {
    luaL_checkversion(L);
    luaL_Reg l[] = {
        { "tree", luatable_tree },
        { NULL, NULL },
    };
    luaL_newlib(L, l);
    return 1;
}
