#include "lua_tableex.h"
#include <stdio.h>
#include <lauxlib.h>
#include <lualib.h>
#include <limits.h>

int main(int argc, const char * argv[]) {
    if (argc != 2) {
        printf("usage: lua-detail file.lua");
        return 1;
    }

    lua_State* l = luaL_newstate();
    luaL_requiref(l, "tableex", luaopen_tableex, 0);
    luaL_openlibs(l);
    
    const char* file = argv[1];
    if (luaL_dofile(l, file))
        printf("[Lua] %s\n", lua_tostring(l, -1));
    
    lua_close(l);
    return 0;
}
